package com.vioside.mrtu;


        import android.app.Activity;
        import android.content.Intent;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.widget.Toast;

        import com.google.android.gms.maps.CameraUpdateFactory;
        import com.google.android.gms.maps.GoogleMap;
        import com.google.android.gms.maps.MapFragment;
        import com.google.android.gms.maps.model.BitmapDescriptorFactory;
        import com.google.android.gms.maps.model.LatLng;
        import com.google.android.gms.maps.model.Marker;
        import com.google.android.gms.maps.model.MarkerOptions;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
        import com.vioside.data.Report;

        import java.util.ArrayList;
        import java.util.Dictionary;
        import java.util.HashMap;
        import java.util.List;

        import io.fabric.sdk.android.Fabric;

public class MapActivity extends Activity implements GoogleMap.OnInfoWindowClickListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "9EaFoSyKOsBNopFP8AvZQqktf";
    private static final String TWITTER_SECRET = "ft8lJLVLcjBo9YxqACA0lY1LQxOiNhIoC3LkZsa64V7RdDNM7F";
    static final LatLng Malta = new LatLng(35.895169259727545, 14.436035281250042);
    private GoogleMap map;
    private ArrayList<Report> reports;
    HashMap<String, Report> reportsForMarkers = new HashMap<String, Report>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new GetReports().execute();

        //Initializes Twitter Stuff here using Fabric
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        setContentView(R.layout.activity_map);
        //Create the map
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();

       //Sets event for infowindow when tapped
        map.setOnInfoWindowClickListener(this);

        // Move the camera instantly to hamburg with a zoom of 15.
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(Malta, 18));

        // Zoom in, animating the camera.
        map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
    }

    public void reloadPins() {
        reportsForMarkers = new HashMap<String, Report>();
        Marker marker;
        for(int i = 0; i < reports.size(); i++)
        {
            int icon = 0;

            if(reports.get(i).getTrafficType() == 1) {
                icon = R.drawable.standstill;
            }
            if(reports.get(i).getTrafficType() == 2) {
                icon = R.drawable.heavy;
            }
            if(reports.get(i).getTrafficType() == 3) {
                icon = R.drawable.medium;
            }


            marker = map.addMarker(new MarkerOptions()
                    .position(new LatLng(reports.get(i).getLatitude(), reports.get(i).getLongitude()))
                    .title(reports.get(i).getTitle())
                    .snippet(reports.get(i).getDescription())
                    .icon(BitmapDescriptorFactory.fromResource(icon)));

            //Create Hashmap to identify marker/report
            reportsForMarkers.put(marker.getId(), reports.get(i));

            //Log.i("map", "added pin: " + reports.get(i).getLatitude() + ", " + reports.get(i).getLongitude());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.Report:
                //Open Report Activity
                Intent intent = new Intent(this, ReportActivity.class);
                startActivity(intent);
                return true;
            case R.id.News:
                //Open News Activity
                Intent newsIntent = new Intent(this, NewsActivity.class);
                startActivity(newsIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Report selected = reportsForMarkers.get(marker.getId());

        Intent intent = new Intent(this, ReportInfoActivity.class);
        intent.putExtra("description",selected.getDescription());
        intent.putExtra("lat", selected.getLatitude());
        intent.putExtra("long", selected.getLongitude());
        intent.putExtra("type", selected.getTrafficType());
        intent.putExtra("title", selected.getTitle());
        startActivity(intent);

        //Toast.makeText(this, selected.getTrafficType() + "" , Toast.LENGTH_LONG).show();
    }

    private class GetReports extends AsyncTask<Void, Void, Integer>
    {
        @Override
        protected Integer doInBackground(Void... params)
        {
            try
            {
                MapActivity.this.reports = Report.GetReports();
            } catch(Exception e)
            {
            }

            return 1;
        }

        protected void onPostExecute(Integer result) {
            MapActivity.this.reloadPins();
        }
    }
}