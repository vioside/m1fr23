package com.vioside.mrtu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.vioside.data.OnlineBus;
import com.vioside.data.Report;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ReportActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private ImageView imageView;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "OpenCV Demo";
    Button sendButton;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    //The Current Report to be sent
    Report report = new Report();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        if(getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        imageView = (ImageView) findViewById(R.id.imageCapture);

        //Default traffic type
        report.setTrafficType(3);

        sendButton = (Button)findViewById(R.id.addReport);

        sendButton.setText("Getting Your Location");
        sendButton.setClickable(false);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(4000); // Update location every second

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("Location", "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("Location", "GoogleApiClient connection has failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        //mLocationView.setText("Location received: " + location.toString());
        //Toast.makeText(this, "Location:" + location.toString(),Toast.LENGTH_LONG).show();
        report.setLatitude(location.getLatitude());
        report.setLongitude(location.getLongitude());
        sendButton.setText("Add Report");
        sendButton.setClickable(true);
        sendButton.setBackgroundResource(R.drawable.mrtubutton);
        sendButton.setTextColor(Color.parseColor("#f37a6b"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intentHome = new Intent(this, MapActivity.class);
                intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentHome);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRadioButtonClicked(View view){

        RadioButton radioButton;

        //Medium Button
        radioButton = (RadioButton) findViewById(R.id.medium);
        if(view.getId() == R.id.medium) {
            radioButton.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.medium,0,0);
            report.setTrafficType(3);
        }
        else{
            radioButton = (RadioButton) findViewById(R.id.medium);
            radioButton.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.mediumnotselected,0,0);
        }

        //Heavy Button
        radioButton = (RadioButton) findViewById(R.id.heavy);
        if(view.getId() == R.id.heavy) {
            radioButton.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.heavy,0,0);
            report.setTrafficType(2);
        }
        else{
            radioButton = (RadioButton) findViewById(R.id.heavy);
            radioButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.heavynotselected, 0, 0);
        }

        //Standstill Button
        radioButton = (RadioButton) findViewById(R.id.standstill);
        if(view.getId() == R.id.standstill) {
            radioButton.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.standstill,0,0);
            report.setTrafficType(1);
        }
        else{
            radioButton = (RadioButton) findViewById(R.id.standstill);
            radioButton.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.standstillnotselected,0,0);
        }

        //Toast.makeText(this,radioButton.getText(),Toast.LENGTH_LONG).show();
    }

    public void takePhoto(View view){
        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //Use local method to create the file
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

        // start the image capture Intent
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    public void addReport(View view){
        //Validation
        EditText description = (EditText)findViewById(R.id.description);
        if(description.getText().length() > 0)
        {
            report.setDescription(description.getText().toString());
        }

        //Toast.makeText(this, "Test", Toast.LENGTH_LONG).show();
        AlertDialog diaBox = AskOption();
        diaBox.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                //Image is saved to local private variable 'fileUri'
                //Toast.makeText(this, "Image saved to:\n" + fileUri.getPath(), Toast.LENGTH_LONG).show();
                ImageView photo = (ImageView)findViewById(R.id.imageCapture);
                Uri link = Uri.parse(fileUri.getPath());
                photo.setImageURI(link);
                report.setImageUrl(link);

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setTitle("Report")
                .setMessage("Are you sure you want to send this report")
                .setIcon(R.drawable.medium)

                .setPositiveButton("Send", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        new UploadReport().execute();
                        dialog.dismiss();
                    }

                })

                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }


    private class UploadReport extends AsyncTask<Void, Void, Integer>
    {
        boolean success;
        @Override
        protected Integer doInBackground(Void... params)
        {
            try
            {
                success = false;
                String locationTitle = ReportInfoActivity.getAddressFromLatLong(report.getLatitude(), report.getLongitude(), ReportActivity.this);
                success = Report.AddReport(locationTitle, "none", report.getDescription(), report.getLatitude(), report.getLongitude(), report.getTrafficType());
            } catch(Exception e)
            {

            }

            return 1;
        }

        protected void onPostExecute(Integer result) {
            Toast.makeText(ReportActivity.this,"Report Sent", Toast.LENGTH_LONG).show();
        }
    }

}
