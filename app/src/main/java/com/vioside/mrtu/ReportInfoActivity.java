package com.vioside.mrtu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ReportInfoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_info);

        Intent intent = getIntent();

        TextView description = (TextView)findViewById(R.id.description);
        if(!intent.getStringExtra("description").isEmpty()) {
            description.setText(intent.getStringExtra("description"));
        }

        TextView address = (TextView)findViewById(R.id.address);
        address.setText(intent.getStringExtra("title"));

        int trafficType = intent.getIntExtra("type", 1);

        TextView trafficTypeText = (TextView)findViewById(R.id.trafficType);
        ImageView trafficTypeImage = (ImageView)findViewById(R.id.trafficTypeImage);

        if(trafficType == 1){
            trafficTypeText.setText("Standstill");
            trafficTypeImage.setImageResource(R.drawable.standstill);
        }
        else if(trafficType == 2){
            trafficTypeText.setText("Heavy");
            trafficTypeImage.setImageResource(R.drawable.heavy);
        }
        else if(trafficType == 3){
            trafficTypeText.setText("Moderate");
            trafficTypeImage.setImageResource(R.drawable.medium);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_report_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static String getAddressFromLatLong(double latitude, double longitude, Context context){
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getAddressLine(1);
    }
}
