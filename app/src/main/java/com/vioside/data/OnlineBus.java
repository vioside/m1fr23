package com.vioside.data;

import android.util.Log;
import android.view.MenuItem;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolegrech on 26/01/15.
 */
public class OnlineBus {

    private static InputStream is = null;
    private static JSONObject jObj = null;
    private static String json = "";

    private static final String domain = "http://mrtuservices.maltesetraffic.com/rest/";

    public static String connectWithRequest(String requestUrl, String method, String data) throws Exception {
        String url = "";
        url = domain + requestUrl;
        Log.d("onlinebus", "url = " + url);
        Log.d("onlinebus", "method = " + method);
        Log.d("onlinebus", "data = " + data);
        URL urls = new URL(url);
        URI uri = new URI(urls.getProtocol(), urls.getUserInfo(), urls.getHost(), urls.getPort(), urls.getPath(), urls.getQuery(), urls.getRef());
        urls = uri.toURL();
        //url = URLEncoder.encode(url,"UFT-8");
        //url = url.replace(' ', '+');

        // Get a httpclient to talk to the internet
        HttpClient client = new DefaultHttpClient();
        // Perform a GET request to YouTube for a JSON list of all the videos by a specific user
        HttpUriRequest request = null;
        if(method.equals("POST")) {
            request = new HttpPost(urls.toString());
            StringEntity se = new StringEntity(data, HTTP.UTF_8);

            se.setContentType("application/json");
            request.setHeader("Content-" +
                    "Type","application/x-www-form-urlencoded;charset=UTF-8");
            ((HttpPost)request).setEntity(se);

        } else if(method.equals("PUT")) {
            request = new HttpPut(urls.toString());
            StringEntity se = new StringEntity(data, HTTP.UTF_8);

            se.setContentType("application/json");
            request.setHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");
            ((HttpPut)request).setEntity(se);

        } else if(method.equals("GET")){
            request = new HttpGet(urls.toString());
        }

        BasicHttpResponse response =
                (BasicHttpResponse) client.execute(request);        // Convert this response into a readable string
        String jsonString = StreamUtils.convertToString(response.getEntity().getContent());

        return jsonString;
    }

}
