package com.vioside.data;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by nicolegrech on 26/01/15.
 */
public class Status {

    public int userId;
    public int state;
    public String title;
    public double longitude;
    public double latitude;

    public boolean uploadStatus() {
        try
        {
            String jsonString = OnlineBus.connectWithRequest("status", "POST", getDescription());
            Log.e("test", jsonString);

            //convert string to json array
            JSONObject jsonData = new JSONObject(jsonString);
            if(jsonData != null) {
                JSONObject statusData = jsonData.getJSONObject("status");
                if(statusData != null) {
                    return true;
                }
            }
        } catch(Exception e)
        {
            Log.e("status", e.toString());
        }

        return false;
    }

    private String getDescription() {
        String data = "userid="+this.userId+"&state="+this.state+"&title="+this.title+"&longitude="+this.longitude+"&latitude="+this.latitude;
        return data;
    }

}
