package com.vioside.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import org.json.JSONObject;

/**
 * Created by nicolegrech on 26/01/15.
 */
public class User {
    public String email;
    public String password;
    public int unique;
    public String name;
    public String emailReceived;

    public String contactsms1;
    public String contactsms2;
    public String contactsms3;
    public String contactsms4;
    public String contactsms5;
    public String contactsmscheck1;
    public String contactsmscheck2;
    public String contactsmscheck3;
    public String contactsmscheck4;
    public String contactsmscheck5;
    public String contactpancake;

    public String active;

    public boolean checkUserLogin(Context context) {
        try {
            SharedPreferences settings = context.getSharedPreferences("ABMSOSPrefs", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("userPassword", password);
            editor.commit();

            String jsonString = OnlineBus.connectWithRequest("user", "POST", "username="+email+"&password="+password);
            if(fillUserData(jsonString)) {
                saveUser(context);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean recheckUserLogin(Context context) {
        try {
            loadUser(context);
            SharedPreferences settings = context.getSharedPreferences("ABMSOSPrefs", 0);
            password = settings.getString("userPassword", null);

            String jsonData = OnlineBus.connectWithRequest("user", "POST", "username="+email+"&password="+password);
            if(fillUserData(jsonData)) {
                saveUser(context);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean hasLogged(Context context) {
        loadUser(context);
        if(this.email != null) {
            return true;
        } else {
            return false;
        }
    }

    private boolean fillUserData(String jsonString) {
        try {
            JSONObject jsonData = new JSONObject(jsonString);
            if (jsonData != null) {
                JSONObject statusData = jsonData.getJSONObject("user");
                if (statusData != null) {
                    name = statusData.getString("name");
                    unique = statusData.getInt("id");
                    emailReceived = statusData.getString("email");
                    contactsms1 = statusData.getString("contactsms1");
                    contactsms2 = statusData.getString("contactsms2");
                    contactsms3 = statusData.getString("contactsms3");
                    contactsms4 = statusData.getString("contactsms4");
                    contactsms5 = statusData.getString("contactsms5");
                    contactsmscheck1 = statusData.getString("contactsmscheck1");
                    contactsmscheck2 = statusData.getString("contactsmscheck2");
                    contactsmscheck3 = statusData.getString("contactsmscheck3");
                    contactsmscheck4 = statusData.getString("contactsmscheck4");
                    contactsmscheck5 = statusData.getString("contactsmscheck5");
                    contactpancake = statusData.getString("contactpancake");
                    active = statusData.getString("active");

                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void saveUser(Context context) {
        // Restore preferences
        // All objects are from android.context.Context
        SharedPreferences settings = context.getSharedPreferences("ABMSOSPrefs", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("userName", name);
        editor.putString("userEmail", email);
        editor.putInt("userUnique", unique);
        editor.putString("contactsms1", contactsms1);
        editor.putString("contactsms2", contactsms2);
        editor.putString("contactsms3", contactsms3);
        editor.putString("contactsms4", contactsms4);
        editor.putString("contactsms5", contactsms5);
        editor.putString("contactsmscheck1", contactsmscheck1);
        editor.putString("contactsmscheck2", contactsmscheck2);
        editor.putString("contactsmscheck3", contactsmscheck3);
        editor.putString("contactsmscheck4", contactsmscheck4);
        editor.putString("contactsmscheck5", contactsmscheck5);
        editor.putString("contactpancake", contactpancake);
        editor.putString("emailReceived", emailReceived);
        editor.commit();
    }

    public void emptyUser(Context context) {
        // Restore preferences
        // All objects are from android.context.Context
        SharedPreferences settings = context.getSharedPreferences("ABMSOSPrefs", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("userName", null);
        editor.putString("userEmail", null);
        editor.putInt("userUnique", -1);
        editor.putString("contactsms1", null);
        editor.putString("contactsms2", null);
        editor.putString("contactsms3", null);
        editor.putString("contactsms4", null);
        editor.putString("contactsms5", null);
        editor.putString("contactsmscheck1", null);
        editor.putString("contactsmscheck2", null);
        editor.putString("contactsmscheck3", null);
        editor.putString("contactsmscheck4", null);
        editor.putString("contactsmscheck5", null);
        editor.putString("emailReceived", null);
        editor.putString("contactpancake", null);
        editor.commit();
    }

    public void loadUser(Context context) {
        // Restore preferences
        // All objects are from android.context.Context
        SharedPreferences settings = context.getSharedPreferences("ABMSOSPrefs", 0);
        name = settings.getString("userName", null);
        email = settings.getString("userEmail", null);
        unique = settings.getInt("userUnique", -1);
        contactsms1 = settings.getString("contactsms1", null);
        contactsms2 = settings.getString("contactsms2", null);
        contactsms3 = settings.getString("contactsms3", null);
        contactsms4 = settings.getString("contactsms4", null);
        contactsms5 = settings.getString("contactsms5", null);
        contactsmscheck1 = settings.getString("contactsmscheck1", null);
        contactsmscheck2 = settings.getString("contactsmscheck2", null);
        contactsmscheck3 = settings.getString("contactsmscheck3", null);
        contactsmscheck4 = settings.getString("contactsmscheck4", null);
        contactsmscheck5 = settings.getString("contactsmscheck5", null);
        contactpancake = settings.getString("contactpancake", null);
        active = settings.getString("active", null);
    }

    public String getContactNumber(int person, boolean sms, int status) {
        String contact = null;

        int pos = ((6*(person-1))+(3*(sms?0:1))+(status-1));
        char permission = contactpancake.charAt(pos);
        try {
            int p = Integer.parseInt(permission + "");

            if (p == 1) {
                switch (person) {
                    case 1:
                        contact = contactsms1;

                        break;
                    case 2:
                        contact = contactsms2;
                        break;
                    case 3:
                        contact = contactsms3;
                        break;
                    case 4:
                        contact = contactsms4;
                        break;
                    case 5:
                        contact = contactsms5;
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {

        }
        return contact;
    }

}
