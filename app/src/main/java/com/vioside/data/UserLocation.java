package com.vioside.data;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by nicolegrech on 27/01/15.
 */
public class UserLocation {
    public int userid;
    public double longitude, latitude;

    public boolean updateLocation() {
        try
        {
            String jsonString = OnlineBus.connectWithRequest("userlocation", "POST", getDescription());
            Log.e("test", jsonString);

            //convert string to json array
            JSONObject jsonData = new JSONObject(jsonString);
            if(jsonData != null) {
                JSONObject statusData = jsonData.getJSONObject("userlocation");
                if(statusData != null) {
                    return true;
                }
            }
        } catch(Exception e)
        {
            Log.e("status", e.toString());
        }

        return false;
    }

    public String getDescription() {
        if(this.userid != 0 && this.longitude != 0 && this.latitude != 0) {
            return "userid="+this.userid+"&longitude="+this.longitude+"&latitude="+this.latitude;
        } else {
            return "";
        }
    }

}
