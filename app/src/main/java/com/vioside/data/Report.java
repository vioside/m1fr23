package com.vioside.data;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by stevescerri on 06/03/15.
 * Traffic Types are as follows:
 * 1 - Standtill
 * 2 - Heavy
 * 3 - Moderate
 */

public class Report {

    private String title;

    private String description;

    private Uri imageUrl;

    private double longitude;

    private double latitude;

    private int trafficType;

    private int userid;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Uri getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Uri imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getLongitude() {  return longitude; }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() { return latitude; }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getTrafficType() {
        return trafficType;
    }

    public void setTrafficType(int trafficType) {
        this.trafficType = trafficType;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public static ArrayList<Report> GetReports(){
        boolean result = false;
        ArrayList<Report> reports = new ArrayList<Report>();
        try {
            String jsonString = OnlineBus.connectWithRequest("report","GET","");
            Log.e("json", jsonString);
            JSONArray jsonArray = new JSONArray(jsonString);

            for (int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonData = jsonArray.getJSONObject(i);

                Report report = new Report();
                if((jsonData.getString("description").contains("null")) || (jsonData.getString("description").equals(null))) {
                    report.setDescription("");
                }
                else{
                    report.setDescription(jsonData.getString("description"));
                }

                if((jsonData.getString("title").contains("null")) || (jsonData.getString("title").equals(null))) {
                    report.setTitle("");
                }
                else{
                    report.setTitle(jsonData.getString("title"));
                }

                report.setLatitude(jsonData.getDouble("latitude"));
                report.setLongitude(jsonData.getDouble("longitude"));
                report.setTrafficType(jsonData.getInt("traffictypeid"));
                report.setImageUrl(Uri.parse("imageurl"));
                reports.add(report);
                Log.d("test", i + " " + jsonData);
            }

            return reports;


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean AddReport(String title, String imageURL, String description, double latitude, double longitude, int trafficType){
        String query = "imageurl=" + imageURL + "&description=" + description + "&title=" + title + "&longitude=" + longitude + "&latitude=" + latitude + "&traffictypeid=" + trafficType;

        try {
            String jsonString = OnlineBus.connectWithRequest("report","POST",query);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
